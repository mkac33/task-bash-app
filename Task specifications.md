Napisz skrypt w bashu, którego zadaniem jest pobranie od użytkownika poniższych informacji. Skrypt powinien walidować dane.
 
1. api-key - minimalnie 20 i maksymalnie 32 znaki alfanumeryczne (litery, cyfry, dozwolone są dwa znaki specjalne -. ). Przykład: a1b2c33d4e5f-6g7h8i9ja.kblc
2. latitude - liczba rzeczywista (max 6 cyfr po przecinku). Przykład: 37.423021
3. longitude - liczba rzeczywista (max 6 cyfr po przecinku). Przykład: -122.083739
4. bandwith - liczba rzeczywista (max 3 cyfry po przecinku) + jednostka. Przykład: 1000 Kbps, 1.5Gbps. Dostępne jednostki: Kbps, Mbps, Gbps. Między liczbą a jednostką może nie być spacji. W docelowym pliku properties liczba i jednostka muszą być rozdzielone spacją.
 
W przypadku błędu skrypt wyświetli informację o błędzie oraz poprosi o ponowne wprowadzenie błędnie wprowadzonego pola (aż do skutku).
Skrypt powienien informować użytkownika jakich danych aktualnie oczekuje.
 
Przykład:
$./skrypt.sh
> Please enter api-key:
a1b2c33d4e5f-6g7h8i9ja.kblc
> Please enter latitude:
37.423021
Jeśli wszystkie dane są poprawne skrypt utworzy plik o nazwie application.properties. Przykład:
api-key=a1b2c33d4e5f-6g7h8i9ja.kblc
latitude=37.423021
longitude=-122.083739
bandwith=1.5 Gbps
 
Ostatnim krokiem jest dodanie do crona (https://manpages.ubuntu.com/manpages/trusty/en/man8/cron.8.html) zadania, które sprawdza co minutę, czy zawartość pliku application.properties została zmieniona. Jeśli plik zmieni się to cron tworzy plik z informacją o różnicy. Miejsce tworzenia pliku jest dowolne. 
