#!/bin/bash

api_key_min_lengh="20"  #minimum lengh of api-key
api_key_max_lengh="32"  #maximium lengh of api-key
coordinate_max_lengh="6"	#maximium lengh of latitude and longtitude
bandwith_max_lengh="3"		#maximium number of characters after comma in bandwith
bandwith_allowed_units_array=(Kbps Mbps Gbps)	#allowed units of bandwith
formatted_bandwith=""		#Formatted version of bandwith
output_file="$PWD/application.properites"  #Output file location
output_file_old="$PWD/application.properites.old"  #Output file location
output_file_difference="$PWD/application.properites.difference"  #Output file location

comma_verifier() {
###########################################################################################
#
#	1)	Verifies if entered string contains "." correctly placed inside it
#	2)	Return error code if it doesn't
#
#	ARGUMENT: string
#	RETURN: 
#		0: Comma is inserted correctly
#		1: Comma is inserted incorrectly
#
###############################################################################################

local comma_location="${1%%"."*}"
[[ "$comma_location" = "$1" ]] && comma_location=0 || comma_location="$((${#comma_location}+1))"	#checing if "." is placed correctly in the string
if  [[ $comma_location -eq 1 ]] || [[ $comma_location -eq ${#1} ]]; then	#checing if "." is not the begining or end of the string		
	return 1
else
	return 0
fi
}

dash_verifier() {
###########################################################################################
#
#	1)	Verifies if entered string contains "-" correctly placed inside it
#	2)	Return error code if it doesn't
#
#	ARGUMENT: string
#	RETURN: 
#		0: Dash is inserted correctly
#		1: Dash is inserted incorrectly
#
###############################################################################################

local dash_location="${1%%"-"*}"
[[ "$dash_location" = "$1" ]] && dash_location=0 || dash_location="$((${#dash_location}+1))"	#checing if "-" is placed correctly in the string
if  [[ $dash_location -gt 1 ]]; then	#checing if "-" is not in the begining of the string
	return 1
else
	return 0
fi
}


api_key_verifier() {
###############################################################################################
#
#	1)	Verifies if Please entered api-key:
#		a) lengh is in required bracket
#		b) has only allowed characters
#	2)	Return error codes depending on error
#	3) 	Creates string if verification is possitive
#
#	GLOBALS:
#		api_key_min_lengh		
#		api_key_max_lengh
#	ARGUMENT: api-key string
#	RETURN: 
#		1: Lengh of api-key is not in required bracket
#		2: Not allowed character present
#	OUTPUT: 
#		string
#
###############################################################################################

if ! ([[ ${#1} -ge $api_key_min_lengh ]] && [[ ${#1} -le $api_key_max_lengh ]]); then		#checking if api-key lengh is in required bracket 	
	return 1
fi

if [[ "$1" =~ [^a-zA-Z0-9.-] ]]; then		#checking if Please entered value is alphanumeric with "." and "-" characters allowed
	return 2
 else
	return 0
fi
}




coordinate_verifier(){
###############################################################################################
#
#	1)	Verifies if Please entered coordinate:
#		a) contains only allowed characters
#		b) value has maximium of allowed number of after comma digits
#	2)	Return error codes depending on error
#	3) 	Creates string if verification is possitive
#
#	GLOBALS:
#		coordinate_max_lengh		
#		formatted_coordinate
#	ARGUMENT: coordinate string
#	RETURN: 
#		1: Not allowed character present
#		2: Comma is inserted incorrectly
#		3: Dash is inserted incorrectly
#		4: After comma digits too long
#	OUTPUT: 
#		string
#
###############################################################################################

if [[ "$1" =~ [^0-9.-] ]]; then  #checking if Please entered value is alphanumeric with "." and "-" characters allowed
	return 1
fi	

if [[ $(grep -o "\." <<<"$1" | wc -l) -gt 1 ]];	then		#checking if string contains not more than one "."
	return 2
fi
		
local comma_verification_error_code="255"
comma_verifier "$1"		#checing if "." is placed correctly in the string
comma_verification_error_code=$?
if [[ comma_verification_error_code -eq 1 ]]; then		#Processing error code
	return 2
fi		

if [[ $(grep -o "\-" <<<"$1" | wc -l) -gt 1 ]];	then		#checking if string contains more than one "-"
	return 3
fi

local dash_verification_error_code="255"
dash_verifier "$1"		#checing if "-" is placed correctly in the string
dash_verification_error_code=$?
if [[ dash_verification_error_code -eq 1 ]]; then		#Processing error code
	return 3
fi
	
local after_comma_coordinate_value=$(cut -d "." -f2 <<< "$1")		#cutting number before comma
if  ([[ ${#after_comma_coordinate_value} -gt $coordinate_max_lengh ]]); then		#checking number of digits after comma
	return 4
fi
	
return 0
}




bandwith_verifier() {
###########################################################################################
#
#	1)	Verifies if entered bandwith:
#		a) contains allowed units
#		b) value is a number
#		c) value has maximium of allowed number of after comma digits
#	2)	Return error codes depending on error
#	3) 	Creates formatted string if verification is possitive
#
#	GLOBALS:
#		bandwith_max_lengh
#		bandwith_allowed_units_array
#		formatted_bandwith
#	ARGUMENT: bandwith string
#	RETURN: 
#		1: Value is non-numeric
#		2: Comma is inserted incorrectly
#		3: Dash is inserted incorrectly
#		4: After comma digits too long
#		5: Not allowed unit present
#	OUTPUT: 
#		Formatted string
#
###############################################################################################

for unit in ${bandwith_allowed_units_array[*]} 	#iteration over allowed bandwith units
do		
	if [[ "$1" == *"$unit" ]]; then		#checking if entered unit is allowed
		
		local bandwith_value=$(echo "$1" | sed 's/ //g')	#removing space between value an unit if it exists			
		bandwith_value=${bandwith_value/%$unit}		#removing unit from string	
		if [[ "$bandwith_value" =~ [^0-9.-] ]]; then		#checing if value is numeric	
			return 1		
		fi
		
		if [[ $(grep -o "\." <<<"$1" | wc -l) -gt 1 ]];	then		#checking if string contains not more than one "."
			return 2
		fi
		

		local comma_verification_error_code="255"
		comma_verifier "$bandwith_value"
		comma_verification_error_code=$?
		if [[ comma_verification_error_code -eq 1 ]]; then		#Processing error code
				return 2
		fi		
		
		if [[ $(grep -o "\-" <<<"$1" | wc -l) -gt 1 ]];	then		#checking if string contains not more than one "-"
			return 3
		fi
		
		local dash_verification_error_code="255"
		dash_verifier "$bandwith_value"		#checing if "-" is placed correctly in the string
		dash_verification_error_code=$?
		if [[ dash_verification_error_code -eq 1 ]]; then		#Processing error code
			return 3
		fi				
		
		local after_comma_bandwith_value=$(cut -d "." -f2 <<< "$bandwith_value")		#counting digits after comma		
		if  ([[ ${#after_comma_bandwith_value} -gt $bandwith_max_lengh ]] && ! [[ $(grep -o "\." <<<"$1" | wc -l) -eq 0 ]]); then		#checking number of digits after comma and if it exists
			return 4		
		else	
			#########################################################################################################
			local dash_location="${1%%"-"*}"																		#
			[[ "$dash_location" = "$1" ]] && dash_location=0 || dash_location="$((${#dash_location}+1))"			#																				
			if [[ $dash_location -eq 1 ]]; then 																	#
				echo "######################################################################"						#
				echo "#           Congratulations on unlocking easter egg!                 #" 						# # small easter egg ;) 
				echo "#           Your bandwith is negative! Call your ISP!                #"						#
				echo "#      Program requirements should be adjusted. Please proceed ;)    #"						#
				echo "######################################################################"						#
			fi																										#
			#########################################################################################################
			formatted_bandwith="${bandwith_value} ${unit}"
			return 0
		fi	
	fi
done
return 5
}




###############################################################################################


clear


api_key_verifier_return_code="255"
for (( ; ; ))		#Api-Key entering loop
do
	if ! [[ api_key_verifier_return_code -eq 0 ]]; then
		read -p "Please enter api-key: `echo $'\n> '`" api_key		#Asking for Api-Key
		api_key_verifier "$api_key"		#Verifying Api-Key
		api_key_verifier_return_code=$?		#Checking if Api_key is correct
			if [[ api_key_verifier_return_code -eq 1 ]]; then		#Processing error code
				echo "Incorrect api-key lengh. Please try again"
			elif [[ api_key_verifier_return_code -eq 2 ]]; then		#Processing error code
				echo "Not allowed character in api-key. Please try again"
			fi
	else
		break		#Exiting loop in case of correct Api-Key
	fi
done

latitude_verifier_return_code="255"
for (( ; ; ))		#latitude entering loop (No range for latitude value in specification)
do
	if ! [[ latitude_verifier_return_code -eq 0 ]]; then
		read -p "Please enter latitude: `echo $'\n> '`" latitude		#Asking for latitude
		coordinate_verifier "$latitude"		#Verifying latitude
		latitude_verifier_return_code=$?	#Checking if latitude is correct
			if [[ latitude_verifier_return_code -eq 1 ]]; then		#Processing error code
				echo "Not allowed character in latitude. Please try again"
			elif [[ latitude_verifier_return_code -eq 2 ]]; then		#Processing error code
				echo "Comma is inserted incorrectly in latitude. Please try again"
			elif [[ latitude_verifier_return_code -eq 3 ]]; then		#Processing error code
				echo "Dash is inserted incorrectly in latitude. Please try again"
			elif [[ latitude_verifier_return_code -eq 4 ]]; then		#Processing error code
				echo "Too many digits after comma in latitude. Please try again"
			fi
	else
		break		#Exiting loop in case of correct latitude
	fi
done

longtitude_verifier_return_code="255"
for (( ; ; ))		#longtitude entering loop	(No range for longtitude value in specification)
do
	if ! [[ longtitude_verifier_return_code -eq 0 ]]; then
		read -p "Please enter longtitude: `echo $'\n> '`" longtitude		#Asking for longtitude
		coordinate_verifier "$longtitude"		#Verifying longtitude
		longtitude_verifier_return_code=$?		#Checking if longtitude is correct
			if [[ longtitude_verifier_return_code -eq 1 ]]; then		#Processing error code
				echo "Not allowed character in longtitude. Please try again"
			elif [[ longtitude_verifier_return_code -eq 2 ]]; then		#Processing error code
				echo "Comma is inserted incorrectly in longtitude. Please try again"
			elif [[ longtitude_verifier_return_code -eq 3 ]]; then		#Processing error code
				echo "Dash is inserted incorrectly in longtitude. Please try again"
			elif [[ longtitude_verifier_return_code -eq 4 ]]; then		#Processing error code
				echo "Too many digits after comma in longtitude. Please try again"
			fi
	else
		break		#Exiting loop in case of correct longtitude
	fi
done


bandwith_verifier_return_code="255"
for (( ; ; ))		#bandwith entering loop
do	
	if ! [[ bandwith_verifier_return_code -eq 0 ]]; then
		read -p "Please enter bandwith: `echo $'\n> '`" bandwith		#Asking for bandwith
		bandwith_verifier "$bandwith"		#Verifying longtitude
		bandwith_verifier_return_code=$?	#Checking if bandwith is correct
			if [[ bandwith_verifier_return_code -eq 1 ]]; then			#Processing error code
				echo "Value of bandwith is incorrect. Please try again"
			elif [[ bandwith_verifier_return_code -eq 2 ]]; then		#Processing error code
				echo "Comma is inserted incorrectly in bandwith. Please try again"
			elif [[ bandwith_verifier_return_code -eq 3 ]]; then		#Processing error code
				echo "Dash is inserted incorrectly in bandwith. Please try again"
			elif [[ bandwith_verifier_return_code -eq 4 ]]; then		#Processing error code
				echo "Too many digits after comma in bandwith. Please try again"
			elif [[ bandwith_verifier_return_code -eq 5 ]]; then		#Processing error code
				echo "Unit of bandwith is incorrect. Please try again"
			fi
	else
		break		#Exiting loop in case of correct bandwith
	fi
done




if [ -f "$output_file" ]; then	
    mv "$output_file" "$output_file.old"
	echo
	echo "Old "$output_file" file has been renamed to "$output_file.old""
fi

#Inserting data to output file
echo "api-key=$api_key" > "$output_file"
echo "latitude=$latitude" >> "$output_file"
echo "longtitude=$longtitude" >> "$output_file"
echo "bandwith=$formatted_bandwith" >> "$output_file"

echo "Data has been saved to: $output_file"

crontab -l > "$output_file.crontab"	#Dumping current cron jobs to file


if ! grep -Fxq "* * * * *  if [ -f "$output_file_old" ]; then diff -y --suppress-common-lines "$output_file" "$output_file_old" > "$output_file_difference"; fi" "$output_file.crontab" #Checking if job is already in cron
then
    echo "* * * * *  if [ -f "$output_file_old" ]; then diff -y --suppress-common-lines "$output_file" "$output_file_old" > "$output_file_difference"; fi" > "$output_file.crontab"		#Inserting cron job to file
	crontab "$output_file.crontab"		#Installing new cron job
	echo "Installing new cron job"
else
	echo "Difference cron job is already installed."

fi
rm -f "$output_file.crontab" 	#Cleaning up temp cron jobs list file